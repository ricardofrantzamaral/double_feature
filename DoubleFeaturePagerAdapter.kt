package br.com.monolit.emusysadmin.utils.double_feature

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.ViewGroup
import br.com.monolit.emusysadmin.submodules.common.requests.conn.APIResponse

class DoubleFeaturePagerAdapter<T : APIResponse>(
        val activity: DoubleFeatureActivity<T>,
        val fragmentManager: FragmentManager,
        val fragmentList: List<DoubleFeatureFragment<T>>) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return fragmentList[position % fragmentList.size]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return fragmentList[position].getTitle()
    }

    override fun setPrimaryItem(container: ViewGroup?, position: Int, `object`: Any?) {
        activity.refreshLayout.isEnabled = fragmentList[position % fragmentList.size].isOnline()
        super.setPrimaryItem(container, position, `object`)
    }
}