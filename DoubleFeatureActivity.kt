package br.com.monolit.emusysadmin.utils.double_feature

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LifecycleRegistry
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import br.com.monolit.androidutils.easydrawer.EasyDrawerActivity
import br.com.monolit.androidutils.misc.PullToRefreshHelper
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.server.APIListener
import br.com.monolit.emusysadmin.submodules.common.requests.conn.APIResponse
import retrofit2.Call

abstract class DoubleFeatureActivity <T : APIResponse> : EasyDrawerActivity(), LifecycleOwner {

    abstract fun setPagerFragments() : List<DoubleFeatureFragment<T>>
    private lateinit var fragments : List<DoubleFeatureFragment<T>>

    private lateinit var contentFrame: ViewGroup
    internal lateinit var refreshLayout: ViewGroup

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: DoubleFeaturePagerAdapter<T>

    private val registry = LifecycleRegistry(this)
    override fun getLifecycle(): Lifecycle = registry
    private var loading = false

    private lateinit var helper: PullToRefreshHelper

    lateinit var liveData: DoubleFeatureLiveData<T>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViews()

        registry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)

        reload()
    }

    private fun setupViews() {
        fragments = setPagerFragments()
        liveData = DoubleFeatureLiveData()

        setContentView(R.layout.activity_double_feature)

        helper = PullToRefreshHelper(findMainFrame(), R.id.double_feature_refresh_layout)
        refreshLayout = findViewById(R.id.double_feature_refresh_layout)
        contentFrame = layoutInflater.inflate(R.layout.double_feature_view_pager_layout, findMainFrame(), false) as ViewGroup
        refreshLayout.addView(contentFrame)
        helper.setOnRefreshListener { reload() }

        viewPager = contentFrame.findViewById(R.id.double_feature_view_pager)
        tabLayout = contentFrame.findViewById(R.id.double_feature_tab)
        tabLayout.setupWithViewPager(viewPager)
        pagerAdapter = DoubleFeaturePagerAdapter(this, supportFragmentManager, fragments)
        viewPager.adapter = pagerAdapter

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {
                enableDisableSwipeRefresh( state == ViewPager.SCROLL_STATE_IDLE )
            }

        })
    }

    abstract fun loadData(listener : APIListener<T>)

    open fun reload() {
        if (loading) {
            return
        }
        loading = true

        liveData.setLoading()

        LoaderSnackbar.hideSnackBar()

        loadData(object : APIListener<T>() {
            override fun onSuccess(result: T) {

                loading = false
                liveData.setSuccess(result)
                helper.setRefreshing(false)
            }

            override fun onFailure(call: Call<T>?, t: Throwable?) {

                loading = false
                if (liveData.getInfo()?.respose != null) {
                    liveData.setOffline()
                } else {
                    liveData.setError(call, t)
                }

                erroAoCarregarDadosOnline()
            }

            override fun onOperationFailure(status: Int, errorMessage: String) {

                loading = false
                if (liveData.getInfo()?.respose != null) {
                    liveData.setOffline()
                } else {
                    liveData.setOfflineNoData()
                }

                erroAoCarregarDadosOnline()
            }
        })
    }

    fun erroAoCarregarDadosOnline() {
        LoaderSnackbar.showErrorLoaderSnackbar(this, this.getString(R.string.error_to_load_data_online), Snackbar.LENGTH_INDEFINITE, object : LoaderSnackbar.OnClickListener {
            override fun onClick(v: View) {
                reload()
            }
        })
    }

    private fun enableDisableSwipeRefresh(enable: Boolean) {
        if (refreshLayout != null) {
            refreshLayout.isEnabled = enable
        }
    }

    override fun onResume() {
        super.onResume()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun onPause() {
        super.onPause()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    }

    override fun onStop() {
        super.onStop()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
    }

    override fun onDestroy() {
        registry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        super.onDestroy()
    }

    private fun findMainFrame(): FrameLayout {
        return findViewById(R.id.online_main_frame) as FrameLayout
    }
}