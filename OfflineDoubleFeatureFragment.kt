package br.com.monolit.emusysadmin.utils.double_feature

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.Meta
import br.com.monolit.emusysadmin.R

abstract class OfflineDoubleFeatureFragment : Fragment(){

    @LayoutRes
    protected abstract fun getContentViewRes(): Int
    abstract fun inicializaViews(view: View)
    abstract fun getTitle(): String

    lateinit var thisView : View
    private lateinit var mainFrame: ViewGroup
    private lateinit var contentFrame: ViewGroup

    override fun getView(): View {
        return thisView
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(DoubleFeatureFragment.TAG, "onCreateView")

        thisView = if (Meta.isDebug()) {
            inflater.inflate(R.layout.double_feature_fragment_debug, container, false)
        } else {
            inflater.inflate(R.layout.double_feature_fragment_basis, container, false)
        }

        contentFrame = activity.layoutInflater.inflate(getContentViewRes(), mainFrame, false) as ViewGroup
        mainFrame.addView(contentFrame)

        inicializaViews(thisView)

        return thisView
    }

    // CAST WARNING, DO NOT MISUSE
    fun getLoadActivity (): OfflineDoubleFeatureActivity {
        return activity as OfflineDoubleFeatureActivity

    }
}