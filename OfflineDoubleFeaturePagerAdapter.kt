package br.com.monolit.emusysadmin.utils.double_feature

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class OfflineDoubleFeaturePagerAdapter (val activity: OfflineDoubleFeatureActivity,
                                        val fragmentManager: FragmentManager,
                                        val fragmentList: List<OfflineDoubleFeatureFragment>) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return fragmentList[position % fragmentList.size]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return fragmentList[position].getTitle()
    }
}