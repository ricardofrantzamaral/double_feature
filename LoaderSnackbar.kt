package br.com.monolit.emusysadmin.utils.double_feature

import android.app.Activity
import android.support.design.widget.Snackbar
import android.view.View
import br.com.monolit.emusysadmin.R

object LoaderSnackbar {
    private var snackBar: Snackbar? = null

    fun showErrorLoaderSnackbar(activity: Activity?, message: String, length: Int, listener: OnClickListener) {
        if (activity != null) {
            val v = activity.window.decorView
            snackBar = getSnackBarErrorfinal(v, message, length, listener)
            snackBar!!.show()
        }
    }

    fun hideSnackBar() {
        if (snackBar != null && snackBar!!.isShown) {
            snackBar!!.dismiss()
        }
    }

    private fun getSnackBarErrorfinal(v: View, message: String, length: Int, listener: OnClickListener): Snackbar {
        val nSnackbar = Snackbar.make(v, message, length)

        nSnackbar.setAction(R.string.try_again, { v -> listener.onClick(v) })

        return nSnackbar
    }

    interface OnClickListener {
        fun onClick(v: View)
    }
}