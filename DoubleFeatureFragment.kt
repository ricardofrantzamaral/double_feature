package br.com.monolit.emusysadmin.utils.double_feature

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.FrameLayout
import br.com.monolit.emusysadmin.Meta
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.submodules.common.requests.conn.APIResponse
import org.jetbrains.annotations.Contract
import retrofit2.Call
import android.content.Context
import android.util.Log


abstract class DoubleFeatureFragment<T : APIResponse> : Fragment() {

    companion object {
        val TAG = DoubleFeatureFragment::class.java.name
    }

    @LayoutRes
    protected abstract fun getContentViewRes(): Int

    abstract fun inicializaViews(view: View)

    private lateinit var mainFrame: ViewGroup
    private lateinit var loadingFrame: ViewGroup
    private lateinit var errorFrame: ViewGroup
    private lateinit var contentFrame: ViewGroup
    private lateinit var offlineFrame: ViewGroup

    private var status = Status.CONTENT

    lateinit var thisView : View

    lateinit var liveData: DoubleFeatureLiveData<T>

    private val observer = object : Observer<DoubleFeatureInfo<T>> {
        override fun onChanged(t: DoubleFeatureInfo<T>?) {
            when (t?.state){
                DoubleFeatureInfo.State.CONTENT -> {
                    if (t.respose != null) {
                        receiveData(t.respose)
                    }
                }
                DoubleFeatureInfo.State.ERROR -> {
                    showFailure(t.call, t.throwable)
                }
                DoubleFeatureInfo.State.LOADING -> {
                    showLoadingScreen()
                }
                DoubleFeatureInfo.State.OFFLINE -> {
                    if (t?.respose != null) {
                        proccessData(t.respose)
                    } else {
                        showOfflineScreen()
                    }
                }
                else ->{
                    showFailure(null, null)
                }
            }
        }
    }

    abstract fun isOnline(): Boolean

    abstract fun getTitle(): String

    override fun getView(): View {
        return thisView
    }

    override fun onAttach(context: Context?) {
        Log.d(TAG, "onAttach")

        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")

        if (isOnline()) {
            liveData = getLoadActivity().liveData
        }

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")

        thisView = if (Meta.isDebug()) {
            inflater.inflate(R.layout.double_feature_fragment_debug, container, false)
        } else {
            inflater.inflate(R.layout.double_feature_fragment_basis, container, false)
        }

        setupViews(thisView)

        inicializaViews(thisView)

        if (isOnline()) {
            val initResult = liveData.getInfo()?.respose
            if (initResult != null) {
                receiveData(initResult)
            } else {
                showLoadingScreen()
            }

            liveData.observe(this, observer)
        } else {
            showContentScreen()
        }

        return thisView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        Log.d(TAG, "onActivityCreated")

        super.onActivityCreated(savedInstanceState)
    }

    override fun onStart() {
        Log.d(TAG, "onStart")

        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG, "onResume")

        super.onResume()
    }

    override fun onPause() {
        Log.d(TAG, "onPause")
        super.onPause()
    }

    override fun onStop() {
        Log.d(TAG, "onStop")
        super.onStop()
    }

    override fun onDestroyView() {
        Log.d(TAG, "onDestroyView")
        if (isOnline()) {
            liveData.removeObserver(observer)
        }
        super.onDestroyView()
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        super.onDestroy()
    }

    override fun onDetach() {
        Log.d(TAG, "onDetach")
        super.onDetach()
    }

    open fun showFailure(call: Call<T>?, t: Throwable?) {
    }

    fun receiveData(data: T){
        showContentScreen()
        proccessData(data)
    }

    abstract fun proccessData(data: T)

    fun onReloadClicked() {
        getLoadActivity().reload()
    }

    @Contract(pure = true)
    private fun getViewForStatus(status: Status): View {
        return when (status) {
            Status.CONTENT -> contentFrame
            Status.ERROR -> errorFrame
            Status.LOADING -> loadingFrame
            Status.OFFLINE -> offlineFrame
        }
    }

    @Contract(pure = true)
    private fun getCurrentShownView(): View {
        return getViewForStatus(status)
    }

    private fun fadeView(view: View) {
        val alphaAnim = AlphaAnimation(1.0f, 0.0f)
        alphaAnim.interpolator = AccelerateDecelerateInterpolator()
        alphaAnim.duration = 200
        alphaAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
            }

            override fun onAnimationEnd(animation: Animation) {
                if (getCurrentShownView() !== view) {
                    view.visibility = View.GONE
                }
            }

            override fun onAnimationRepeat(animation: Animation) {
            }
        })
        view.startAnimation(alphaAnim)
    }

    private fun changeScreenStatus(newStatus: Status) {
        if (status != newStatus) {
            val currentView = getCurrentShownView()
            val viewForStatus = getViewForStatus(newStatus)
            setViewToTop(viewForStatus)
            setViewToTop(currentView)
            mainFrame.invalidate()
            viewForStatus.visibility = View.VISIBLE
            fadeView(currentView)
        }
        status = newStatus
    }

    protected fun showContentScreen() {
        changeScreenStatus(Status.CONTENT)
    }

    protected fun showLoadingScreen() {
        changeScreenStatus(Status.LOADING)
    }

    protected fun showErrorScreen() {
        changeScreenStatus(Status.ERROR)
    }

    protected fun showOfflineScreen() {
        changeScreenStatus(Status.OFFLINE)
    }

    private fun setViewToTop(view: View) {
        mainFrame.bringChildToFront(view)
    }

    private fun findMainFrame(view: View): FrameLayout {
        return view.findViewById(R.id.online_main_frame) as FrameLayout
    }

    private fun setupViews(view: View) {
        mainFrame = findMainFrame(view)
        loadingFrame = view.findViewById(R.id.online_loading_frame)
        errorFrame = view.findViewById(R.id.online_error_frame)
        contentFrame = activity.layoutInflater.inflate(getContentViewRes(),
                mainFrame, false) as ViewGroup
        mainFrame.addView(contentFrame)

        //contentFrame.visibility = View.GONE
        errorFrame.visibility = View.GONE
        loadingFrame.visibility = View.GONE
        errorFrame.setOnClickListener(View.OnClickListener { onReloadClicked() })
    }

    protected enum class Status {
        CONTENT, LOADING, OFFLINE, ERROR
    }

    // CAST WARNING, DO NOT MISUSE
    fun getLoadActivity (): DoubleFeatureActivity<T> {
        return activity as DoubleFeatureActivity<T>
    }
}