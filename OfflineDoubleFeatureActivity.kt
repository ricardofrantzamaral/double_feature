package br.com.monolit.emusysadmin.utils.double_feature

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleRegistry
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import br.com.monolit.androidutils.easydrawer.EasyDrawerActivity
import br.com.monolit.androidutils.misc.PullToRefreshHelper
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.server.APIListener
import br.com.monolit.emusysadmin.submodules.common.requests.conn.APIResponse
import retrofit2.Call

abstract class OfflineDoubleFeatureActivity : EasyDrawerActivity() {
    abstract fun setPagerFragments() : List<OfflineDoubleFeatureFragment>
    private lateinit var fragments : List<OfflineDoubleFeatureFragment>

    private lateinit var contentFrame: ViewGroup

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: OfflineDoubleFeaturePagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        fragments = setPagerFragments()

        setContentView(R.layout.activity_double_feature)

        contentFrame = layoutInflater.inflate(R.layout.double_feature_view_pager_layout, findMainFrame(), false) as ViewGroup

        viewPager = contentFrame.findViewById(R.id.double_feature_view_pager)
        tabLayout = contentFrame.findViewById(R.id.double_feature_tab)
        tabLayout.setupWithViewPager(viewPager)
        pagerAdapter = OfflineDoubleFeaturePagerAdapter(this, supportFragmentManager, fragments)
        viewPager.adapter = pagerAdapter
    }

    private fun findMainFrame(): FrameLayout {
        return findViewById(R.id.online_main_frame) as FrameLayout
    }
}