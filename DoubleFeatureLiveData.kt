package br.com.monolit.emusysadmin.utils.double_feature

import android.arch.lifecycle.LiveData
import android.support.annotation.MainThread
import br.com.monolit.emusysadmin.server.APIListener
import br.com.monolit.emusysadmin.submodules.common.requests.conn.APIResponse
import retrofit2.Call
import retrofit2.Response

class DoubleFeatureLiveData<T : APIResponse> : LiveData<DoubleFeatureInfo<T>>(){
    init {
        value = DoubleFeatureInfo()
    }

    fun setSuccess(response: T?) {
        postValue(DoubleFeatureInfo(respose = response,
                state = DoubleFeatureInfo.State.CONTENT))
    }

    fun setError(call: Call<T>?, t: Throwable?) {
        postValue(DoubleFeatureInfo(call = call,
                state = DoubleFeatureInfo.State.ERROR, throwable = t))
    }

    fun setLoading() {
        postValue(DoubleFeatureInfo(state = DoubleFeatureInfo.State.LOADING))
    }

    fun setOfflineNoData() {
        postValue(DoubleFeatureInfo(respose = value?.respose,
                state = DoubleFeatureInfo.State.OFFLINE))
    }

    fun setOffline() {
        postValue(DoubleFeatureInfo(state = DoubleFeatureInfo.State.OFFLINE))
    }

    fun getInfo() : DoubleFeatureInfo<T>? {
        return value
    }
}