package br.com.monolit.emusysadmin.utils.double_feature

import br.com.monolit.emusysadmin.submodules.common.requests.conn.APIResponse
import retrofit2.Call

class DoubleFeatureInfo<T: APIResponse> (val respose: T? = null,
                                         val state: State = State.LOADING,
                                         val call: Call<T>? = null,
                                         val throwable: Throwable? = null) {

    enum class State {
        CONTENT, LOADING, OFFLINE, ERROR
    }
}